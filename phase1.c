/**
 * phase1.c
 * Part A
 *
 * @author: Hercy (Yunhao Zhang)
 * @author: David Carrig
 *
 */

#include <stddef.h>
#include "usloss.h"
#include "phase1.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* -------------------------- Globals ------------------------------------- */
#define DEBUG_FLAG      1 // Debug flag


#define MAX_PRIORITY    6

#define UNUSED         -100

#define INVALID        -1
#define RUNNING         0
#define READY           1
#define KILLED          2
#define QUIT            3
#define WATING          4


USLOSS_Context prev;



typedef struct PCB
{
    char name[20];
    int PID;
    int parentPID;
    int priority;
    int numOfChildren;
    int time;
    USLOSS_Context context;
    int state;
    void *stack;
    int stackSize;
    int (*startFunc)(void *);   /* Starting function */
    void *startArg;             /* Arg to starting function */
} PCB;

int i, j;                   // Super useful index i & j
/* Leave it here for now, need to be changed for future usage */
typedef struct interrupt    // Interrupt Type
{
    int code;
    char name[20];
} interrupt;
interrupt *IV;              // Interrupt Vector

struct processNode
{
    int PID;
    struct processNode *next;
};

struct processNode *readyList [MAX_PRIORITY];
struct processNode *blockedList;


/* The process table */
PCB procTable[P1_MAXPROC];


/* Current process ID */
int pid = -1;

/* Number of processes */

int numProcs = 0;

static int sentinel(void *arg);
static void launch(void);

/* -------------------------- Functions ----------------------------------- */

/* ------------------------------------------------------------------------
 Name - insertFront
 Purpose - Insert element to the front of the given linked list
 ----------------------------------------------------------------------- */
void insertFront(struct processNode **list, int newPID)
{
    struct processNode *tmp;
    tmp = (struct processNode *)malloc(sizeof(struct processNode));
    tmp->PID = newPID;
    
    if(*list == NULL)
    {
        *list = (struct processNode *)malloc(sizeof(struct processNode));
        *list = tmp;
        (*list)->next = NULL;
    }
    else
    {
        tmp->next = *list;
        *list = tmp;
    }
}

/* ------------------------------------------------------------------------
 Name - insertRear
 Purpose - Insert element at the rear of the given linked list
 ----------------------------------------------------------------------- */
void insertRear(struct processNode **list, int newPID)
{
    struct processNode *aProcess = (struct processNode *)malloc(sizeof(struct processNode));
    aProcess->PID = newPID;
    aProcess->next = NULL;
    
    struct processNode *tmp = *list;

    if(*list == NULL)
    {
        *list = (struct processNode *)malloc(sizeof(struct processNode));
        *list = aProcess;
    }
    else
    {
        while(tmp->next)
        {
            tmp = tmp->next;
        }

        tmp->next = aProcess;
    }
}

/* ------------------------------------------------------------------------
 Name - delete
 Purpose - Delete element from the given linked list
 ----------------------------------------------------------------------- */
int delete(struct processNode **list, int somePID)
{
    struct processNode *tmp = *list;
    struct processNode *freer;

    if(*list == NULL)
    {
        return -1;
    }
    else if(tmp->next == NULL)
    {
        if(tmp->PID == somePID)
        {
            freer = *list;
            if(freer != NULL)
            {
                free(freer);
            }
            *list = NULL;
            return somePID;
        }
    }
    else
    {
        struct processNode *current;

        while(tmp->next)
        {
            if(tmp->PID == somePID)
            {
                freer = tmp;
                if(NULL) // TODO: I'll fix you later
                {
                    free(freer);
                }
                *tmp = *tmp->next;
                return somePID;
            }
            current = tmp;
            tmp = tmp->next;
        }

        if(tmp->PID == somePID)
        {
            freer = current->next;
            if(freer != NULL)
            {
                free(freer);
            }
            current->next = NULL;
            return somePID;
        }
    }
    return -1;
}


/* ------------------------------------------------------------------------
 Name - printLinkedList
 Purpose - Print the given linked list
 ----------------------------------------------------------------------- */
void printLinkedList(struct processNode *list)
{
    if(DEBUG_FLAG)
    {
        //USLOSS_Console("*** @printLinkedList(): Printing linked list...\n");
    }
    struct processNode *tmp = list;
    while(tmp)
    {
        printf("     PID: %d - %s\n", tmp->PID, procTable[tmp->PID].name);
        tmp = tmp->next;
    }
    if(DEBUG_FLAG)
    {
        //USLOSS_Console("     Print linked list FINISHED\n");
    }
}

void printReadyList()
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @printReadyList(): Printing ready list...\n");
    }
    int i;
    for(i = 0; i < MAX_PRIORITY; i++)
    {
        printf("     Priority %d:\n", i + 1);
        printLinkedList(readyList[i]);
    }

    if(DEBUG_FLAG)
    {
        USLOSS_Console("     Print ready list FINISHED\n");
    }
}

int readyListDelete(int somePID)
{
    int i;
    int errorChecker = 0;
    for(i = 0; i < MAX_PRIORITY; i++)
    {
        if(delete(&readyList[i], somePID) == somePID)
        {
            errorChecker++;
        }
    }

    if(errorChecker > 0)
    {
        return -2; // Duplicates found, huge problem here.
    }
    if(errorChecker == 0)
    {
        return -1;  // Just simply can't find it.
    }

    return somePID;
}

/* ------------------------------------------------------------------------
 Name - updateReadyList
 Purpose - Self-update the ready list array with process priorities
 ----------------------------------------------------------------------- */
void updateReadyList()
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @updateReadyList(): Updating Ready list... ");
    }
    int j = 0;
    for(i = 0; i < MAX_PRIORITY; i++)
    {
        for(j = 0; j < P1_MAXPROC; j++)
        {
            if(procTable[j].PID != -1 && procTable[j].priority == i + 1)
            {
                insertFront(&readyList[i], procTable[j].PID);
            }
        }
    }
    
    // Test delete sentinal
    //delete(&readyList[5], 0);

    if(DEBUG_FLAG)
    {
        USLOSS_Console("OK\n");
    }
    
    if(DEBUG_FLAG)
    {
        printf("*** @updateReadyList(): Calling printLinkedList(readyList[%d])\n", i);
        for(i = 0; i < MAX_PRIORITY; i++)
        {
            //printLinkedList(readyList[i]);
        }
    }
}

/* ------------------------------------------------------------------------
 Name - P1_GetPID
 Purpose - Return the PID of the currently running process
 ----------------------------------------------------------------------- */
int P1_GetPID()
{
    return pid;
}

/* ------------------------------------------------------------------------
 Name - P1_GetState
Returns:   -1: invalid PID
            0: the process is running
            1: the process is ready
            2: the process has been killed
            3: the process has quit
            4: the process is waiting on a semaphore
 ----------------------------------------------------------------------- */
int P1_GetState(int PID)
{
    return procTable[PID].state;
}

/* ------------------------------------------------------------------------
 Name - P1_DumpProcesses
 Purpose - Print process information list
 ----------------------------------------------------------------------- */
void P1_DumpProcesses()
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_DumpProcesses(): Start printing process table...\n");
    }
    PCB pcb;
    
    for(i = 0; i < P1_MAXPROC; i++)
    {
        pcb = procTable[i];
        if(pcb.PID >= 0)
        {
            USLOSS_Console("     PID: %d, Parent's PID: %d, Priority: %d, State: %d, Children: %d, CPU Time Consumed: %d - %s\n",
                           pcb.PID,
                           pcb.parentPID,
                           pcb.priority,
                           pcb.state,
                           pcb.numOfChildren,
                           pcb.time,
                           pcb.name);
        }
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console("     Print process table FINISHED\n");
    }
}

int checkPrioritiy()
{
    int i;
    for(i = 0; i < MAX_PRIORITY; i++)
    {
        struct processNode *tmp = readyList[i];
        while(tmp)
        {
            if(procTable[tmp->PID].state == RUNNING || procTable[tmp->PID].state == READY)
            {
                return i;
            }
            tmp = tmp->next;
        }
/*
        if(readyList[i].state == RUNNING || readyList[i].state == READY)
        {
            return i;
        }
 */
    }
    return MAX_PRIORITY - 1;
}

/* ------------------------------------------------------------------------
 Name - dispatcher
 Purpose - runs the highest priority runnable process
 Parameters - none
 Returns - nothing
 Side Effects - runs a process
 ----------------------------------------------------------------------- */
void dispatcher()
{
    /*
     * Run the highest priority runnable process. There is guaranteed to be one
     * because the sentinel is always runnable.
     */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @dispatcher(): Starting...\n");
    }

    int i;
    for(i = 0; i < MAX_PRIORITY; i++)
    {

        while(readyList[i])
        {
            if(DEBUG_FLAG)
            {
                USLOSS_Console("*** @dispatcher(): Running at priority %d...\n", i + 1);
            }
            struct processNode *tmp = readyList[i];
            while(tmp)
            {
                int aPID = tmp->PID;        
                pid = aPID;
                if(DEBUG_FLAG)
                {
                    USLOSS_Console("     Current process: %d - %s\n", pid, procTable[aPID].name);
                }
                if(DEBUG_FLAG)
                {
                    P1_DumpProcesses();
                }
                switch(procTable[aPID].state)
                {
                    case READY:
                        procTable[aPID].state = RUNNING;
                        if(DEBUG_FLAG)
                        {
                            USLOSS_Console("     Running: %s...\n", procTable[aPID].name);
                        }
                        USLOSS_ContextSwitch(&prev, &procTable[aPID].context);
                        
                        //procTable[aPID].context = prev;
                        //dispatcher();
                        i = checkPrioritiy();
                        break;
                    /*
                    case QUIT:
                        procTable[aPID].state = UNUSED;
                        procTable[aPID].PID = -1;
                        int x = readyListDelete(aPID);
                        USLOSS_Console("         Quitting: %s...\n", procTable[aPID].name);
                        if(x == -1)
                        {
                            USLOSS_Console("         Unable to delete %d.\n", aPID);
                        }
                        break;
                    */
                    case KILLED:
                        P1_Quit(KILLED);
                        break;

                    case UNUSED:
                        USLOSS_Console("     Should never run an UNUSED process slot!!!\n");
                        break;
                }
                /*
                if(DEBUG_FLAG)
                {
                    P1_DumpProcesses();
                }
                 */
                tmp = tmp->next;
            }
        }
        

    }
    
    USLOSS_Console("*** @dispatcher(): Should never see this!!! ERROR!\n");
    
}

/* ------------------------------------------------------------------------
 Name - startup
 Purpose - Initializes semaphores, process lists and interrupt vector.
 Start up sentinel process and the P2_Startup process.
 Parameters - none, called by USLOSS
 Returns - nothing
 Side Effects - lots, starts the whole thing
 ----------------------------------------------------------------------- */
void startup()
{
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Checking kernel mode... ");
    }
    if(USLOSS_PsrGet() != 1)
    {
        USLOSS_Console("ERROR!\n");
        USLOSS_Console("     NOT in kernel mode!\n");
        USLOSS_Console("     Halting...\n");
        USLOSS_Halt(1);
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console("OK\n");
    }
    
    /* Initialize the process table */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the process table...");
    }
    for(i = 0; i < P1_MAXPROC; i++)
    {
        procTable[i].PID = -1;
        procTable[i].state = UNUSED;
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    if(DEBUG_FLAG && pid == 0)
    {
        USLOSS_Console(" OK\n");
    }
    
    /* Initialize the Ready list, Blocked list, etc. */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the Ready list, Blocked list...");
    }
    for(i = 0; i < MAX_PRIORITY; i++)
    {
        readyList[i] = NULL;
    }
    blockedList = NULL;
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    /* Initialize the interrupt vector */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the Interrupt Vector...");
    }
    IV = NULL;
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    /* Initialize the semaphores here */
    // TODO
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the semaphores...");
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" SKIPPED!\n");
    }
    
    
    /* Startup a sentinel process */
    /* HINT: you don't want any forked processes to run until startup is finished.
     * You'll need to do something in your dispatcher to prevent that from happening.
     * Otherwise your sentinel will start running right now and it will call P1_Halt.
     */
    P1_Fork("sentinel", sentinel, NULL, USLOSS_MIN_STACK, 6);
    
    /* Start the P2_Startup process */
    P1_Fork("P2_Startup", P2_Startup, NULL, 4 * USLOSS_MIN_STACK, 1);
    
    /* Test */
    /*
    P1_Fork("P3_Startup", P2_Startup, NULL, 4 * USLOSS_MIN_STACK, 2);
    P1_Fork("P4_Startup", P2_Startup, NULL, 4 * USLOSS_MIN_STACK, 2);
    P1_Fork("P5_Startup", P2_Startup, NULL, 4 * USLOSS_MIN_STACK, 3);
    */
    /* Print process table */
    //P1_DumpProcesses();
    
    /* Update the ready list */
    //updateReadyList();

    //printReadyList();

    //readyListDelete(3);
    //printReadyList();
    
    dispatcher();
    
    /* Should never get here (sentinel will call USLOSS_Halt) */
    
    return;
} /* End of startup */

/* ------------------------------------------------------------------------
 Name - finish
 Purpose - Required by USLOSS
 Parameters - none
 Returns - nothing
 Side Effects - none
 ----------------------------------------------------------------------- */
void finish()
{
    USLOSS_Console("Goodbye.\n");
} /* End of finish */

int findEmptyPID()
{
    int emptyPID = -1;
    
    int i;
    for(i = 0; i < P1_MAXPROC; i++)
    {
        if(procTable[i].PID == -1 && procTable[i].state == UNUSED)
        {
            return i;
        }
    }
    
    if(emptyPID == -1)
    {
        USLOSS_Console("*** @findEmptyPID(): Unable to find a empty slot.\n");
    }

    return emptyPID;
}

/* ------------------------------------------------------------------------
 Name - P1_Fork
 Purpose - Gets a new process from the process table and initializes
 information of the process.  Updates information in the
 parent process to reflect this child process creation.
 Parameters - the process procedure address, the size of the stack and
 the priority to be assigned to the child process.
 Returns - the process id of the created child or an error code.
 Side Effects - ReadyList is changed, procTable is changed, Current
 process information changed
 ------------------------------------------------------------------------ */
int P1_Fork(char *name, int (*f)(void *), void *arg, int stacksize, int priority)
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_Fork(): Initializing current process: %s...", name);
    }
    
    /* newPid = pid of empty PCB here */
    int newPid = findEmptyPID();
    
    void *stack = NULL;
    stack = malloc(stacksize);
    strcpy(procTable[newPid].name, name);
    
    procTable[newPid].PID = newPid;
    procTable[newPid].parentPID = pid;
    procTable[newPid].priority = priority;
    procTable[newPid].numOfChildren = 0;
    procTable[newPid].stackSize = stacksize;
    procTable[newPid].state = READY;
    procTable[newPid].stack = stack;

    procTable[newPid].startFunc = f;
    procTable[newPid].startArg = arg;
    
    // Add to ready list
    insertRear(&readyList[priority - 1], newPid);
    
    USLOSS_ContextInit(&(procTable[newPid].context), USLOSS_PsrGet(), stack, stacksize, launch);
    
    if(pid > -1)
    {
        procTable[pid].numOfChildren++;
    }

    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    if(pid > -1 && priority < procTable[pid].priority)
    {
        /*if(prev) // Check NULL
        {
            USLOSS_ContextSwitch(&prev, &prev);
        }*/
        //int PIDclone = pid;
        //pid = newPid;
        //USLOSS_ContextSwitch(&(procTable[PIDclone].context), &prev);
        //dispatcher();
    }
    
    return newPid;
} /* End of fork */

/* ------------------------------------------------------------------------
 Name - launch
 Purpose - Dummy function to enable interrupts and launch a given process
 upon startup.
 Parameters - none
 Returns - nothing
 Side Effects - enable interrupts
 ------------------------------------------------------------------------ */
void launch(void)
{
    int rc;
    USLOSS_PsrSet(USLOSS_PsrGet() | USLOSS_PSR_CURRENT_INT);

    rc = procTable[pid].startFunc(procTable[pid].startArg);
    /* quit if we ever come back */
    P1_Quit(rc);
} /* End of launch */

/* ------------------------------------------------------------------------
 Name - P1_Quit
 Purpose - Causes the process to quit and wait for its parent to call P1_Join.
 Parameters - quit status
 Returns - nothing
 Side Effects - the currently running process quits
 ------------------------------------------------------------------------ */
void P1_Quit(int status)
{

    
    if(procTable[pid].state == UNUSED)// || procTable[pid].state == QUIT)
    {
         USLOSS_Console("Unable to quit, this prcoess has already quit.\n");
    }
    else
    {
        if(DEBUG_FLAG)
        {
            P1_DumpProcesses();
        }
        
        int tmpPID = pid;
        
        // I'm child, I quitted.
        int parentPID = procTable[tmpPID].parentPID;
        if(parentPID != -1)
        {
            (procTable[parentPID].numOfChildren)--;
            /*
            if(procTable[parentPID].state != QUIT)
            {
                (procTable[parentPID].numOfChildren)--;
            }
             */
        }

        // I'm parent, I quitted.
        int numOfChildren = procTable[tmpPID].numOfChildren;
        if(numOfChildren > 0)
        {
            int i;
            for(i = 0; i < P1_MAXPROC; i++)
            {
                if(procTable[i].parentPID == pid)
                {
                    procTable[i].parentPID = -1;
                }
            }
        }
        
        
        // Remove it from ready list
        int x = readyListDelete(tmpPID);
        if(DEBUG_FLAG)
        {
            USLOSS_Console("*** @P1_Quit(): Quitting: %s...", procTable[tmpPID].name);
        }
        if(x == -1)
        {
            USLOSS_Console("Unable to delete %d from the ready list.\n", pid);
        }

        
        // Set current pid into parent's pid
        pid = procTable[tmpPID].parentPID;
        
        // Remove it from process table
        procTable[tmpPID].state = QUIT;
        //procTable[tmpPID].PID = -1;
        //procTable[tmpPID].state = UNUSED;
        if(DEBUG_FLAG)
        {
            USLOSS_Console(" OK\n");
        }
    }

    //dispatcher();
    USLOSS_ContextSwitch(NULL, &prev);
}

/* ------------------------------------------------------------------------
 P1_Kill()
 Returns:  -2: PID belongs to the current process
           -1: invalid PID
            0: success
 ------------------------------------------------------------------------ */
int P1_Kill(int PID)
{
    // Invalid PID, sentinal shall NOT be killed.
    if(procTable[PID].state == UNUSED || PID == 0)
    {
        return INVALID;
    }
    
    // PID belongs to the current process
    if(PID == pid)
    {
        return -2;
    }
    
    procTable[PID].state = KILLED;
    return 0;
}

/* ------------------------------------------------------------------------
 Name - P1_Join
 Returns:   -1: the process doesn’t have any children
          >= 0: the PID of the child that quit
 ------------------------------------------------------------------------ */
int P1_Join(int *status)
{
    
    // TODO
    return -1;
}



/* ------------------------------------------------------------------------
 Name - sentinel
 Purpose - The purpose of the sentinel routine is two-fold.  One
 responsibility is to keep the system going when all other
 processes are blocked.  The other is to detect and report
 simple deadlock states.
 Parameters - none
 Returns - nothing
 Side Effects -  if system is in deadlock, print appropriate error
 and halt.
 ----------------------------------------------------------------------- */
int sentinel(void *notused)
{
    while(numProcs > 1)
    {
        /* Check for deadlock here */
        USLOSS_WaitInt();
    }
    USLOSS_Halt(0);
    /* Never gets here. */
    return 0;
} /* End of sentinel */
